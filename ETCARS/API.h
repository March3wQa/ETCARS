#pragma once
#include "prerequisites.h"
class API
{
private:
	std::string CAInfoPath();
	std::string capath;
public:
	API();
	~API();
	std::string get(std::string url, std::map<std::string,std::string> &params);
	std::string post(std::string url, std::map<std::string, std::string> &params);
	std::string get(std::string url);
	std::string post(std::string url);
	static size_t WriteCallback(void *contents, size_t size, size_t nmemb, char *&userp);
};