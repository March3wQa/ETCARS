#pragma once
#include "API.h"
API::API()
{
	capath = CAInfoPath();
};
API::~API()
{

};
std::string API::CAInfoPath()
{
	std::string path = "";
#ifdef _WIN32
	if (const char* env_p = std::getenv("ETCARS_CA_BUNDLE"))
	{
		path = std::string(env_p);
	}
	else
		path = "cacert.pem";
#endif

#if defined(__linux__)
	path = "/etc/ssl/certs/ca-certificates.crt";
#endif

#if defined(__APPLE__)
	if (const char* env_p = std::getenv("HOME"))
	{
		path = std::string(env_p);
		path += "/Documents/ETCARS/cacert.pem";
	}
	else
		path = "cacert.pem";
#endif
	return path;
}

std::string API::get(std::string url, std::map<std::string, std::string> &params)
{
	std::string res = "";


	return res;
}
std::string API::post(std::string url, std::map<std::string, std::string> &params)
{
	char* response = new char[0];
	std::string result = "";

	long resCode;
	CURLcode res;
	CURL* curl;
	curl = curl_easy_init();

	struct curl_slist *slist1;
	slist1 = NULL;
	slist1 = curl_slist_append(slist1, "Content-Type: application/x-www-form-urlencoded");
	slist1 = curl_slist_append(slist1, "accept: */*");
	slist1 = curl_slist_append(slist1, "accept-language: en-US,en;q=0.8");
	std::string encodedData = "";
	int index = 0;
	for (auto const &item : params)
	{
		if (strlen(encodedData.c_str()) != 0)
			encodedData += "&";
		encodedData += item.first;
		encodedData += "=";
		encodedData += curl_easy_escape(curl, item.second.c_str(), (int)std::strlen(item.second.c_str()));
		index++;
	}
	std::string contentLength = "Content-Length:";
	contentLength += std::to_string((int)std::strlen(encodedData.c_str()));
	const char* fieldData = encodedData.c_str();
	slist1 = curl_slist_append(slist1, contentLength.c_str());

	curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist1);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &API::WriteCallback);
	curl_easy_setopt(curl, CURLOPT_USERAGENT, "ETCARS/0.15");
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 2L);//2L
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 2L);//2L
	curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, (long)std::strlen(encodedData.c_str()));
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, fieldData);
	curl_easy_setopt(curl, CURLOPT_POST, 1);
#if defined(_WIN32) || defined(__APPLE__)
	curl_easy_setopt(curl, CURLOPT_CAINFO, capath.c_str());
#endif
	res = curl_easy_perform(curl);
	if (res != CURLE_OK)
	{
		result = "{";
		result += "\"error\": true,";
		result += "\"errorMessage\": \"Server Error\",";
		result += "\"response\": \"" + std::string(response) + "\",";
		result += "\"url\": \"" + url + "\",";
		result += "\"curlMessage\": \"";
		result += curl_easy_strerror(res);
		result += "\"";
		result += "}";
	}
	else
	{
		curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &resCode);
		if (resCode == 200)
			result = std::string(response);
		else
		{
			result = "{\"error\": true,\"errorMessage\": \"Server Error\", \"errorCode\": ";
			result += std::to_string(resCode);
			result += "}";
		}
	}
	curl_easy_cleanup(curl);
	url = "";
	response = "";
	encodedData = "";
	fieldData = "";
	contentLength = "";

	return result;
}

std::string API::get(std::string url)
{
	char* response = new char[0];
	std::string result = "";

	long resCode;
	CURLcode res;
	CURL* curl;
	curl = curl_easy_init();
	curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &API::WriteCallback);
	curl_easy_setopt(curl, CURLOPT_USERAGENT, "ETCARS/0.15");
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 2L);//2L
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 2L);//2L
	#if defined(_WIN32) || defined(__APPLE__)
	curl_easy_setopt(curl, CURLOPT_CAINFO, capath.c_str());
	#endif
	res = curl_easy_perform(curl);
	if (res != CURLE_OK)
	{
		result = "{";
		result += "\"error\": true,";
		result += "\"errorMessage\": \"Server Error\",";
		result += "\"response\": \"" + std::string(response) + "\",";
		result += "\"url\": \"" + url + "\",";
		result += "\"curlMessage\": \"";
		result += curl_easy_strerror(res);
		result += "\"";
		result += "}";
		
	}
	else
	{
		curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &resCode);
		if (resCode == 200)
			result = std::string(response);
		else
		{
			result = "{\"error\": true,\"errorMessage\": \"Server Error\"}";
		}
	}
	curl_easy_cleanup(curl);
	url = "";
	response = "";
	
	return result;
}

std::string API::post(std::string url)
{
	std::string response = "";

	return response;
}

size_t API::WriteCallback(void *contents, size_t size, size_t nmemb, char *&userp)
{
	std::stringstream ss;
	ss << userp;
	ss << (char*)contents;
	delete[] userp;
	int len = (int)std::strlen(ss.str().c_str()) + 1;
	userp = new char[len];
	strncpy(userp, ss.str().c_str(), len);
	ss.str("");
	return size * nmemb;
}