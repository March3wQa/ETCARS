if [ -f "/home/jammerxd/Projects/ETCARS/ETCARS/ETCARSx64.so" ]; then
   rm "/home/jammerxd/Projects/ETCARS/ETCARS/ETCARSx64.so"
fi



#if [ ! -f "/home/jammerxd/Projects/Dependencies/openssl-1.1.1-pre4/build-x64/lib/libssl.a"]
#BUILD OPENSSL x64:
#./config --prefix=/home/jammerxd/Projects/Dependencies/openssl-1.1.1-pre4/build-x64 --openssldir=/home/jammerxd/Projects/Dependencies/openssl-1.1.1-pre4/ssl --release no-deprecated no-shared disable-weak-ssl-ciphers no-ssl no-ssl2 no-ssl3
#make
#make test
#make install
#make clean
#fi

#if [ ! -f "/home/jammerxd/Projects/Dependencies/curl-7.59.0/build-x64/lib/libcurl.a"]
#BUILD CURL  x64:
#LDFLAGS="-static" PKG_CONFIG="pkg-config --static" LDFLAGS="-L/home/jammerxd/Projects/Dependencies/openssl-1.1.1-pre4/build-x64/lib" ./configure --with-ssl=/home/jammerxd/Projects/Dependencies/openssl-1.1.1-pre4/build-x64 --disable-shared --enable-static --prefix=/home/jammerxd/Projects/Dependencies/curl-7.59.0/build-x64 --disable-ldap --disable-sspi --without-librtmp --disable-ftp --disable-file --disable-dict --disable-telnet --disable-tftp --disable-rtsp --disable-pop3 --disable-imap --disable-smtp --disable-gopher --disable-smb --without-libidn --without-zlib --disable-libcurl-option
#make
#make test
#make install
#make clean
#fi

#BUILD BOOST
#tools/build/b2 install --build-type=complete --build-dir=/users/jammerxd/Desktop/Projects/Dependencies/boost_1_66_0/build-x64 -a variant=release threading=multi address-model=64 runtime-link=static link=static cxxflags="-fPIC" --layout=versioned
#


#64-bit

g++ -w *.cpp \
 /home/jammerxd/Projects/Dependencies/curl-7.59.0/build-x64/lib/libcurl.a \
 /home/jammerxd/Projects/Dependencies/openssl-1.1.1-pre4/build-x64/lib/libssl.a \
 /home/jammerxd/Projects/Dependencies/openssl-1.1.1-pre4/build-x64/lib/libcrypto.a \
 -lpthread \
 -DCURL_STATICLIB  \
 -o ETCARSx64.so \
 -I/home/jammerxd/Projects/Dependencies/curl-7.59.0/build-x64/include \
 -I/home/jammerxd/Projects/Dependencies/scs_sdk_1.9/include \
 -I/home/jammerxd/Projects/Dependencies/scs_sdk_1.9/include/common \
 -I/home/jammerxd/Projects/Dependencies/scs_sdk_1.9/include/amtrucks \
 -I/home/jammerxd/Projects/Dependencies/scs_sdk_1.9/include/eurotrucks2 \
 -I/usr/local/include \
 -I/usr/include \
 -I/home/jammerxd/Projects/Dependencies/sdk_135/public \
 -I/home/jammerxd/Projects/Dependencies/rapidjson/include \
 -I/usr/local/boost/fPIC/include \
 -I/home/jammerxd/Projects/Dependencies/cryptopp \
 -L/usr/local/boost/fPIC/lib \
 -lboost_thread-gcc54-mt-sd-x64-1_66 \
 -lboost_system-gcc54-mt-sd-x64-1_66 \
 -lboost_regex-gcc54-mt-sd-x64-1_66 \
 -lboost_iostreams-gcc54-mt-sd-x64-1_66 \
 -L/home/jammerxd/Projects/Dependencies/sdk_135/redistributable_bin/linux64 \
 -lsteam_api \
 -L/home/jammerxd/Projects/Dependencies/cryptopp/build/x64 \
 -lcryptopp \
 -fPIC -Wall --shared -pthread -std=c++11 -m64

if [ -f "/home/jammerxd/Projects/ETCARS/ETCARS/ETCARSx64.so" ]; then
   rm "/home/jammerxd/.steam/steam/steamapps/common/American Truck Simulator/bin/linux_x64/plugins/ETCARSx64.so"
   cp "/home/jammerxd/Projects/ETCARS/ETCARS/ETCARSx64.so" "/home/jammerxd/.steam/steam/steamapps/common/American Truck Simulator/bin/linux_x64/plugins/ETCARSx64.so"
   rm "/home/jammerxd/.steam/steam/steamapps/common/Euro Truck Simulator 2/bin/linux_x64/plugins/ETCARSx64.so"
   cp "/home/jammerxd/Projects/ETCARS/ETCARS/ETCARSx64.so" "/home/jammerxd/.steam/steam/steamapps/common/Euro Truck Simulator 2/bin/linux_x64/plugins/ETCARSx64.so"
fi
