#include "Placement.h"
Placement::Placement()
{
	this->x = 0.00f;
	this->y = 0.00f;
	this->z = 0.00f;
	this->heading = 0.00f;
	this->pitch = 0.00f;
	this->roll = 0.00f;
}
Placement::Placement(float x, float y, float z, float heading, float pitch, float roll)
{
	this->x = x;
	this->y = y;
	this->z = z;
	this->heading = heading;
	this->pitch = pitch;
	this->roll = roll;
}
float Placement::GetX()
{
	if(!this) return 0.00f;
	return this->x;
}
float Placement::GetY()
{
	if(!this) return 0.00f;
	return this->y;
}
float Placement::GetZ()
{
	if(!this) return 0.00f;
	return this->z;
}
float Placement::GetHeading()
{
	if(!this) return 0.00f;
	return this->heading;
}
float Placement::GetRoll()
{
	if(!this) return 0.00f;
	return this->roll;
}
float Placement::GetPitch()
{
	if(!this) return 0.00f;
	return this->pitch;
}

void Placement::SetX(float val)
{
	if(!this) return;
	if(!val) return;
	this->x = val;
}
void Placement::SetY(float val)
{
	if(!this) return;
	if(!val) return;
	this->y = val;
}
void Placement::SetZ(float val)
{
	if(!this) return;
	if(!val) return;
	this->z = val;
}
void Placement::SetHeading(float val)
{
	if(!this) return;
	if(!val) return;
	this->heading = val;
}
void Placement::SetPitch(float val)
{
	if(!this) return;
	if(!val) return;
	this->pitch = val;
}
void Placement::SetRoll(float val)
{
	if(!this) return;
	if(!val) return;
	this->roll = val;
}
