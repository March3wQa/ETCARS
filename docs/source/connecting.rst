##############
Receiving Data
##############

| To receive data from the game, simply connect to the TCP Socket. The plugin uses port 30001 for clients to connect on.
| There are several events and different messages that are broadcast with different structures.
| Apart from events, data is broadcast regularly at 1 second intervals.

*****
Putty
*****
| Putty is a basic shell program that can connect to various levels of protocols and is available free of charge at `http://putty.org <http://putty.org>`_.
| To view the raw data, you can open a putty window on the same machine with the game running. The host is ``localhost`` and the port is ``30001``. The connection type is raw.
| Then you can see the data as it's broadcast in real time. You may notice the data is in JSON format which is easily decodable in multiple languages.
| To make it look pretty, you can copy all of it to the clipboard, then use a website like `JSON Lint <http://jsonlint.com>`_ to make it look nice.

***********
Programming
***********
| To receive data from ETCARS, your programming language should connect using  standard TCP socket. 
| The host is ``localhost`` and the port is ``30001``. 
| Once connected, you should look to your socket receive or async receive method. This will capture the data broadcast.

********************
Third Party Packages
********************
| There are many other languages out there that have the ability to connect and receive ETCARS's data. Note that none of these are officially supported by the development team of ETCARS and are maintained at the sole discretion of their authors.

- JavaScript: https://www.npmjs.com/package/etcars-node-client
- C#: https://github.com/shardick/etcars-dotnet-sdk
